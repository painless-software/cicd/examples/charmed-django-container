# Contributing

Use this containerization setup for developing with Docker Compose.
It sets up all dependent services for developing, automatically.

## Prerequisites

[Install Docker][docker] on your development machine.

## Local Development

Rename or copy the `example.env` file to `.env` and update its values.

If you want to use the image built by the CI/CD pipeline instead of building
it locally, log in to your [GitLab container registry][gitlab-registry] with
a [personal access token][gitlab-pat] , e.g.

```console
docker login registry.gitlab.com
```

Use [Docker Compose][compose] to run Django commands, e.g.

```console
docker compose up
```

```console
docker compose exec application manage.py makemigrations
```

Migrations will run automatically at startup (via the container entrypoint).
If they fail the very first time simply restart the application.

Open your web browser at http://localhost:8000 to see the application
you're developing. Log output will be displayed in the terminal, as usual.

Use `docker compose down` and the `docker volume` commands to tear down and
clean up your setup again.

## Testing

Functional tests and vulnerability checks run in the
[CI/CD pipeline][pipeline]'s `test` stage using the freshly built
container image.

If you need to do local troubleshooting of the Django application try:

```console
docker run --rm \
  -e DJANGO_ALLOWED_HOSTS='*' \
  -e DJANGO_DATABASE_URL=sqlite:// \
  -e DJANGO_TESTING=pytest \
  -p 8000:8000
```

## Development Process

### Branching Model

This project implements a process with simple, powerful topic branching
and optional review apps. You're encouraged to use `main` as your default
branch.

1. Start a topic branch (e.g. `feature/awesome-thing`, `fix/query-bug`).
1. Make code changes and add related tests.
1. Open a merge request (MR).
1. Merge the MR after code review.

The [CI/CD pipeline][pipeline] will build a container image and push it
to the [local container registry][gitlab-registry]. The image is tagged
with the MR number (e.g. `mr123`) when created from an active merge
request, and with `main` when built from the default branch.

### Releases

For deployments to production a stable version is created when a release
is issued in the [charmed-django][package] repository.

[docker]: https://docs.docker.com/get-docker/
[compose]: https://docs.docker.com/get-started/08_using_compose/
[pipeline]: .gitlab-ci.yml
[gitlab-pat]: https://gitlab.com/-/profile/personal_access_tokens
[gitlab-registry]: https://gitlab.com/painless-software/cicd/examples/charmed-django-container/container_registry
[package]: https://gitlab.com/painless-software/cicd/examples/charmed-django
