# Charmed Django

Containerization of Charmed Django.

This project builds the image container of the `charmed-django` application.
See the [Releases][releases] section in the contribution guide for details.

## Getting Started

See [CONTRIBUTING][contrib] for instructions on development and testing.

[releases]: CONTRIBUTING.md#releases
[contrib]: CONTRIBUTING.md
