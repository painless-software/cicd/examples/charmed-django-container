ARG DOCKER_REGISTRY=docker.io

FROM ${DOCKER_REGISTRY}/library/python:3.10-slim-bullseye

ARG PACKAGE=charmed-django
ARG PIP_INDEX_URL=https://pypi.org/simple

ENV HOME=/home/python \
    PYTHONIOENCODING=UTF-8 \
    PYTHONUNBUFFERED=1 \
    USER=python

WORKDIR ${HOME}

COPY . .

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections \
 && apt-get update \
 && apt-get upgrade -y \
 && apt-get install -y --no-install-recommends \
    libpq5 \
    build-essential \
    libpq-dev \
    python3-dev \
 && PIP_DISABLE_PIP_VERSION_CHECK=1 \
    PIP_INDEX_URL=${PIP_INDEX_URL} \
    PIP_NO_CACHE_DIR=1 \
    pip install ${PACKAGE} \
 && apt-get purge --autoremove -y \
    build-essential \
    libpq-dev \
    python3-dev \
 && apt-get autoremove --purge -y \
 && apt-get autoclean \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/* \
 && DJANGO_SECRET_KEY=collectstatic \
    manage.py collectstatic --noinput --link \
 && useradd -u 1001 -U ${USER} \
# Support arbitrary user IDs (OpenShift guidelines)
 && chown -R ${USER}:0 ${HOME} \
 && chmod -R g=u       ${HOME}

USER ${USER}

HEALTHCHECK --interval=1m --timeout=5s --retries=2 --start-period=10s \
  CMD python -c 'from urllib import request as r; f = r.urlopen("http://localhost:8000/healthz"); raise SystemExit(0 if f.status == 200 else f"{f.status} {f.reason}")'

ENTRYPOINT ["./entrypoint.sh"]

ENV GUNICORN_CMD_ARGS="-b 0.0.0.0:8000 -k gthread -w 2 --worker-tmp-dir /dev/shm --threads 4 --log-level info --log-file -"

CMD ["gunicorn", "application.wsgi"]
